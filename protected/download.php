<?php require_once('../config.php');
require_once(BASE_PATH.'/manage-site/manage-site-common.php');
//ini_set('include_path',BASE_PATH.'/includes/');
if(($loginUrl = checkLogin()) !== true) { header('Location: '.$loginUrl); exit; }

//for empty page get the home page
//echo $_GET['page'].' level :'.$_SESSION['level']; exit();
if(trim($_GET['page'])=='index' && !isset($_GET['file'])) $_GET['page']='home';

if (isset($_GET['page'])){
	if (($_SESSION['level'] == 1) && (strpos($_GET['page'],'dataroom') === 0 OR strpos($_GET['page'],'dataroom') > 0 ) ) { header('Location: ../index.php?error=1'); exit; } //exit;
	else{
		 $file_name = $_GET['page'].'.php';
		 $file_path = BASE_PATH.'/protected/'.$file_name;
	         if(file_exists($file_path))
		 {
		     include_once($file_path); 
		     $sql = 'INSERT INTO log_pages VALUES ('.$_SESSION['login_id'].', NOW(), \''.mysql_real_escape_string($file_name).'\');';
		     $db->query($sql);
		 }
		 else
		 {
		     die('Invalid file location');
		 }    
	}
}



if (isset($_GET['file'])){
	$file = BASE_PATH.'/'.$_GET['file']; 
	//echo $file; var_dump(file_exists($file)); exit();
	if (file_exists($file)) {
	        //echo $file.' base namee  '.basename(str_replace('"', '\\"', $file)); exit();
		$download = array();
		$download = explode ('/', $_GET['file']);
		$cat = str_replace('/'.end($download), '', $_GET['file']); 
		$cat_name =  end ( explode ('/', $cat) );
		//echo $cat_name; exit();
		$sql = 'INSERT INTO log_download VALUES ('.$_SESSION['login_id'].', NOW(), \''.end($download).'\', \''.mysql_real_escape_string($cat_name).'\');';
		//echo $sql;
		$db->query($sql);
	    header('Content-Description: File Transfer');
	    header('Content-Type: application/octet-stream');
	    header('Content-Disposition: attachment; filename="'.basename(str_replace('"', '\\"', $file)).'"');
	    header('Content-Transfer-Encoding: binary');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($file));
	    //echo 'a' ;exit();
	    //ob_clean();
	    //flush();
	    readfile($file);
	    echo $file;	    
	    exit;	    
	}
}

?>