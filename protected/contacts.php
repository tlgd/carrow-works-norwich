<?php
 require_once('../config.php');
require_once(BASE_PATH.'/manage-site/manage-site-common.php');
    //ini_set('include_path',BASE_PATH.'/includes/');
    if(($loginUrl = checkLogin()) !== true) { header('Location: '.$loginUrl); exit; }

// from http://uk.php.net/filesize
function format_bytes($a_bytes)
{
    if ($a_bytes < 1024) {
        return $a_bytes .' B';
    } elseif ($a_bytes < 1048576) {
        return round($a_bytes / 1024, 2) .' KB';
    } elseif ($a_bytes < 1073741824) {
        return round($a_bytes / 1048576, 2) . ' MB';
    } elseif ($a_bytes < 1099511627776) {
        return round($a_bytes / 1073741824, 2) . ' GB';
    } elseif ($a_bytes < 1125899906842624) {
        return round($a_bytes / 1099511627776, 2) .' TB';
    } elseif ($a_bytes < 1152921504606846976) {
        return round($a_bytes / 1125899906842624, 2) .' PB';
    } elseif ($a_bytes < 1180591620717411303424) {
        return round($a_bytes / 1152921504606846976, 2) .' EB';
    } elseif ($a_bytes < 1208925819614629174706176) {
        return round($a_bytes / 1180591620717411303424, 2) .' ZB';
    } else {
        return round($a_bytes / 1208925819614629174706176, 2) .' YB';
    }
}

function outputFiles($path)
{
    $output = '';
    //$handle = @opendir($path);
    //while (false !== ($entry = @readdir($handle))) {
    //    if ($entry != '.' && $entry != '..') {
    //        $output .= '<li><a href="'.SITE_URL.'/protected/download.php?file=protected/'.urlencode($path).'/'.urlencode($entry).'" title="'.htmlspecialchars($entry).'" target="_blank">'.htmlspecialchars($entry.' - '.format_bytes(filesize($path.'/'.$entry))).'</a></li>';
    //    }
    //}
    foreach (glob($path.'/*.*') as $filename) {
        $outputFilename = str_replace($path.'/', '', $filename);
        $output .= '<li><a href="'.SITE_URL.'/protected/download.php?file=protected/'.urlencode($path).'/'.urlencode($outputFilename).'" title="'.htmlspecialchars($outputFilename).'" target="_blank">'.htmlspecialchars($outputFilename.' - '.format_bytes(filesize($filename))).'</a></li>';
    }
    if ($output == '') {
        $output .= '<li>Sorry, no files currently; please check back soon</li>';
    }
    echo $output;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Morello London - East Croydon Station - Contacts</title>

<meta name="keywords" content="Morello, London, East Croydon Station, Development, Mixed use scheme, Regeneration">
<meta name="description" content="Menta is offering an opportunity to invest in a prestigious mixed-use development in croydon, one of London's most vibrant retail and commercial hubs.">

<link href="css/bootstrap.css" rel="stylesheet">


<link href="css/morello-style.css" type="text/css" rel="stylesheet" />



<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-34161773-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

</head>

<body>

<div id="background" >
	
    <img alt="" src="images/background.jpg" >
    
    </div>
    
    <div id="nav">
    <h1>Morello London - East Croydon Station</h1>

<?php $sections = array('architect_plans' => 'Architect Plans'); ?>

     <ul class="nav nav-tabs nav-stacked" id="myTab">
<?php //foreach($sections as $ref => $title) echo '<a href="#'.urlencode($ref).'">'.htmlspecialchars($title).'</a>'; ?>
		<li><a href="dataroom.php#Architect_Plans">Architect Plans</a></li>
		<li><a href="dataroom.php#Area_Schedules" >Area Schedules</a></li>
		<li><a href="dataroom.php#Building_Cost">Building Cost</a></li>
		<li><a href="dataroom.php#CGI">CGI</a></li>
		<li><a href="dataroom.php#Drawings">Drawings</a></li>
		<li><a href="dataroom.php#EIA">EIA</a></li>
		<li><a href="dataroom.php#Fly_Through">Fly Through</a></li>
		<li><a href="dataroom.php#Legal_Documents">Legal Documents</a></li>
		<li><a href="dataroom.php#Materials_and_Design">Materials and Design</a></li>
		<li><a href="dataroom.php#Planning">Planning</a></li>
		<li><a href="dataroom.php#Planning_Surveys_and_Statements">Planning Surveys and Statements</a></li>
		<li><a href="dataroom.php#Research">Research</a></li>
	</ul>  
    
    <p><a href="brochure.pdf" style="margin-left:30px;"><strong>Download Brochure</strong></a></p>

    <p><a href="contacts.php" style="margin-left:30px;"><strong style="color:#FFF;">Contacts</strong></a></p>

    
    </div>
    
    <div class="header" >
    <h2>Contacts</h2>
    </div>

<?php //foreach($sections as $ref => $title) echo '<a href="#'.urlencode($ref).'">'.htmlspecialchars($title).'</a>'; ?>


   <div class="tab-content"> 
    <div class="tab-pane active">
<ul class="mcol">
<li>
<strong style="float:none; padding-bottom:0;">Chris Lacey</strong><br />
Executive Director<br />
t: +44(0) 207 182 2318<br />
e: <a href="mailto:chris.lacey@cbre.com?subject=Morello London">chris.lacey@cbre.com</a>
</li>
<li>
<strong style="float:none; padding-bottom:0;">Rebecca Shum</strong> (Hong Kong)<br />
Executive Director<br />
t: +852 28202980<br />
e: <a href="mailto:rebecca.shum@cbre.com.hk?subject=Morello London">rebecca.shum@cbre.com.hk</a>
</li>
<li>
<strong style="float:none; padding-bottom:0;">Antonio Marin-Bataller</strong><br />
Director<br />
t: +44(0) 207 182 2406<br />
e: <a href="mailto:antonio.marin-bataller@cbre.com?subject=Morello London">antonio.marin-bataller@cbre.com</a>
</li>
</ul>

<ul class="mcol">
<li>
<strong style="float:none; padding-bottom:0;">Alex Davis</strong><br />
Senior Surveyor<br />
t: +44(0) 207 182 2486<br />
e: <a href="mailto: alex.davis@cbre.com?subject=Morello London">alex.davis@cbre.com</a>
</li>
<li>
<strong style="float:none; padding-bottom:0;">Hamer Boot</strong><br />
Senior Surveyor<br />
t: +44(0) 207 182 2274<br />
e: <a href="mailto:hamer.boot@cbre.com?subject=Morello London">hamer.boot@cbre.com</a>
</li>
</ul>

</div>

    
 

</div>

    
    <div id="menta">
    Menta
    </div>
    


</body>
</html>
