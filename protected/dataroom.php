<?php
error_reporting(0);
ini_set('display_errors', 0);

 require_once('../config.php');
require_once(BASE_PATH.'/manage-site/manage-site-common.php');
    //ini_set('include_path',BASE_PATH.'/includes/');
    if(($loginUrl = checkLogin()) !== true) { header('Location: '.$loginUrl); exit; }

// from http://uk.php.net/filesize
function format_bytes($a_bytes)
{
    if ($a_bytes < 1024) {
        return $a_bytes .' B';
    } elseif ($a_bytes < 1048576) {
        return round($a_bytes / 1024, 2) .' KB';
    } elseif ($a_bytes < 1073741824) {
        return round($a_bytes / 1048576, 2) . ' MB';
    } elseif ($a_bytes < 1099511627776) {
        return round($a_bytes / 1073741824, 2) . ' GB';
    } elseif ($a_bytes < 1125899906842624) {
        return round($a_bytes / 1099511627776, 2) .' TB';
    } elseif ($a_bytes < 1152921504606846976) {
        return round($a_bytes / 1125899906842624, 2) .' PB';
    } elseif ($a_bytes < 1180591620717411303424) {
        return round($a_bytes / 1152921504606846976, 2) .' EB';
    } elseif ($a_bytes < 1208925819614629174706176) {
        return round($a_bytes / 1180591620717411303424, 2) .' ZB';
    } else {
        return round($a_bytes / 1208925819614629174706176, 2) .' YB';
    }
}

function outputFiles($path)
{
    $output = '';
    //$handle = @opendir($path);
    //while (false !== ($entry = @readdir($handle))) {
    //    if ($entry != '.' && $entry != '..') {
    //        $output .= '<li><a href="'.SITE_URL.'/protected/download.php?file=protected/'.urlencode($path).'/'.urlencode($entry).'" title="'.htmlspecialchars($entry).'" target="_blank">'.htmlspecialchars($entry.' - '.format_bytes(filesize($path.'/'.$entry))).'</a></li>';
    //    }
    //}
    foreach (glob($path.'/*.*') as $filename) {    	
        $outputFilename = str_replace($path.'/', '', $filename);
        $output .= '<li><a href="'.SITE_URL.'/protected/download.php?file=protected/'.urlencode($path).'/'.urlencode($outputFilename).'" title="'.htmlspecialchars($outputFilename).'" target="_blank">'.htmlspecialchars($outputFilename.' - '.format_bytes(filesize($filename))).'</a></li>';
    }
    if ($output == '') {
        $output .= '<li>Sorry, no files currently; please check back soon</li>';
    }
    echo $output;
}

function outputImages($path)
{
    $output = '';
    //$handle = @opendir($path);
    //while (false !== ($entry = @readdir($handle))) {
    //    if ($entry != '.' && $entry != '..') {
    //        $output .= '<li><a href="'.SITE_URL.'/protected/download.php?file=protected/'.urlencode($path).'/'.urlencode($entry).'" title="'.htmlspecialchars($entry).'" target="_blank">'.htmlspecialchars($entry.' - '.format_bytes(filesize($path.'/'.$entry))).'</a></li>';
    //    }
    //}
    foreach (glob($path.'/*.*') as $filename) {  
        $outputFilename = str_replace($path.'/', '', $filename);
        $output .= '<li><a href="'.SITE_URL.'/protected/download.php?file=protected/'.urlencode($path).'/large/'.urlencode($outputFilename).'" title="'.htmlspecialchars($outputFilename).'" target="_blank"><img src="'.SITE_URL.'/protected/download.php?file=protected/'.urlencode($path).'/'.urlencode($outputFilename).'" alt="'.htmlspecialchars($outputFilename).'"></a></li>';
    }
    if ($output == '') {
        $output .= '<li>Sorry, no files currently; please check back soon</li>';
    }
    echo $output;
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>
<?=$title;?>
- Data room - Commercial Property Agency &amp; Consultancy <?= $client ?></title>
<meta name="keywords" content="<?= $client ?>">
<meta name="description" content="Data rooms for Commercial Property Agency &amp; Consultancy <?= $client ?>">
<link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
<link href="css/custom.css" type="text/css" rel="stylesheet" />

<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

<script type="text/javascript" src="//use.typekit.net/rww1sfn.js"></script>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
</head>

<body class="nobg">
<nav>
<div class="container">
<div class="row">
<div class="span4">
    <h1 id="logoTitle"><span>CARROW</span><br/><span>WORKS</span><br/><span>NORWICH</span></h1>
</div>
<div class="span5 logos">
    <a href="http://www.cushmanwakefield.co.uk/en-gb"><img class="cushman" src="../protected/images/cushman logo.png" /></a>
    <a href="http://www.harveyandco.com/" id="harveyco">HARVEY&CO</a>
</div>
<div class="span2">
<ul>
<li><a href="/index.php?logout=1" class="contact" title="<?= $client ?> Chartered Surveyors - Log Out" >Log Out</a></li>
</div>
</div>
</div>
</nav>
<div class="container">
    <div class="row">
        <div class="span4">
            <?php //$sections = array('architect_plans' => 'Architect Plans'); ?>
            <ul class="nav nav-tabs nav-stacked" id="myTab">
                <li class="active top top-level"><a href="#tab1">Existing Buildings - Floor plans and Elevations</a></li>
                <li class="top-level"><a href="#tab2">Planning</a></li>
                <li class="top-level"><a href="#tab3">Site Plan</a></li>
                <li class="top-level"><a href="#tab4">Technical and Environmental Information</a></li>
                <li class="top-level"><a href="#tab5">Title Documentation</a></li>
                <li class="top-level"><a href="#tab6">Viewing Protocol</a></li>
                <li class="top-level"><a href="#tab7">Bid Proforma</a></li>
            </ul>
            <p class="added">You will need to have adobe acrobat installed to view some of these documents, <a href="http://www.adobe.com/products/acrobat.html" target="_blank">click here</a> if you do not have it installed</p>
            <!--<a class="btnD" href="downloads/Keybridge-Brochure.pdf">Download Brochure</a>--> 
            <div id="contactInfo">
                <h3>Cushman & Wakefield Contact</h3>
                <p>Daniel McDonagh<br/>
                Tel: <a href="tel:020 3296 4674">020 3296 4674</a><br/>
                Email: <a href="mailto:daniel.mcdonagh@cushwake.com?subject=Carrow Works, Norwich - Enquiry">daniel.mcdonagh@cushwake.com</a></p>
                <p>George Jolliffe<br/>
                Tel: <a href="tel:020 3296 2734">020 3296 2734</a><br/>
                Email: <a href="mailto:george.jolliffe@cushwake.com?subject=Carrow Works, Norwich - Enquiry">george.jolliffe@cushwake.com</a><p>
                <h3>Harvey & Co Contact</h3>
                <p>David Harvey<br/>
                Tel: <a href="tel:01603 677162">01603 677162</a><br/>
                Email: <a href="mailto:david@harveyandco.com?subject=Carrow Works, Norwich - Enquiry">david@harveyandco.com</a></p>
            </div>
            </div>    
        <div class="span7">
            <?php //foreach($sections as $ref => $title) echo '<a href="#'.urlencode($ref).'">'.htmlspecialchars($title).'</a>'; ?>
            <div class="tab-content">

                <div class="tab-pane active" id="tab1">
                    <h2>Existing Buildings - Floor plans and Elevations</h2>
                        <ul>
                            <?php outputFiles('download/Existing Buildings - Floorplans and Elevations'); ?>
                        </ul>
                    <h3>Existing Building - Accommodation Schedule</h3>
                    <ul>
                        <?php outputFiles('download/Existing Buildings - Floorplans and Elevations/Existing Building - Accommodation Schedule'); ?>
                    </ul>
                </div>

                <div class="tab-pane" id="tab2">
                    <h2>Planning</h2>
                    <ul>
                        <?php outputFiles('download/Planning'); ?>
                    </ul>
                    <h3>CIL Relief Statement</h3>
                    <ul>
                        <?php outputFiles('download/Planning/CIL Relief Statement'); ?>
                    </ul>
                </div>

                <div class="tab-pane" id="tab3">
                    <h2>Site Plan</h2>
                    <ul>
                        <?php outputFiles('download/Site Plan'); ?>
                    </ul>
                </div>

                <div class="tab-pane" id="tab4">
                    </ul>
                   <h3>Arboricultural Appraisal</h3>
                    <ul>
                        <?php outputFiles('download/Technical and Environmental Information/Arboricultural Appraisal'); ?>
                    </ul>
                    <h3>Archaeological Assessment</h3>
                    <ul>
                        <?php outputFiles('download/Technical and Environmental Information/Archaeological Assessment'); ?>
                    </ul>
                    <h3>Ecology Survey</h3>
                    <ul>
                        <?php outputFiles('download/Technical and Environmental Information/Ecology Survey'); ?>
                    </ul>
                    <h3>Flood Risk Analysis</h3>
                    <ul>
                        <?php outputFiles('download/Technical and Environmental Information/Flood Risk Analysis'); ?>
                    </ul>
                    <h3>Feasibility Study</h3>
                    <ul>
                        <?php outputFiles('download/Technical and Environmental Information/Feasibility Study'); ?>
                    </ul>
                    <h3>Heritage Analysis</h3>
                    <ul>
                        <?php outputFiles('download/Technical and Environmental Information/Heritage Analysis'); ?>
                    </ul>
                    <h3>Site Investigation Reports (Phase I & II)</h3>
                    <ul>
                        <?php outputFiles('download/Technical and Environmental Information/Site Investigation Reports (Phase I & II)'); ?>
                    </ul>
                    <h3>Topographical Survey</h3>
                    <ul>
                        <?php outputFiles('download/Technical and Environmental Information/Topographical Survey'); ?>
                    </ul>
                    <h3>Transport Analysis</h3>
                    <ul>
                        <?php outputFiles('download/Technical and Environmental Information/Transport Analysis'); ?>
                    </ul>
                    <h3>Utilities Survey</h3>
                    <ul>
                        <?php outputFiles('download/Technical and Environmental Information/Utilities Survey'); ?>
                    </ul>
                </div>

                <div class="tab-pane" id="tab5">
                    <h3>Title Matters Summary</h3>
                    <ul>
                        <?php outputFiles('download/Title Documentation/Title Matters Summary'); ?>
                    </ul>
                   <!-- <h3>Council Transfer Plan</h3>
                    <ul>
                        <?php outputFiles('download/Title Documentation/Council Transfer Plan'); ?>
                    </ul> -->
                    <h3>NK175840</h3>
                    <ul>
                        <?php outputFiles('download/Title Documentation/NK175840'); ?>
                    </ul>
                    <h3>NK175854</h3>
                    <ul>
                        <?php outputFiles('download/Title Documentation/NK175854'); ?>
                    </ul>
                    <h3>NK178582</h3>
                    <ul>
                        <?php outputFiles('download/Title Documentation/NK178582'); ?>
                    </ul>
                    <h3>NK205133</h3>
                    <ul>
                        <?php outputFiles('download/Title Documentation/NK205133'); ?>
                    </ul>
                    <h3>NK298417</h3>
                    <ul>
                        <?php outputFiles('download/Title Documentation/NK298417'); ?>
                    </ul>
                    <h3>NK345301</h3>
                    <ul>
                        <?php outputFiles('download/Title Documentation/NK345301'); ?>
                    </ul>
                </div>
                 <div class="tab-pane" id="tab6">
                    <h3>Viewing Protocol</h3>
                    <ul>
                        <?php outputFiles('download/Viewing Protocol'); ?>
                    </ul>
                </div>
                <div class="tab-pane" id="tab7">
                    <h3>Bid Proforma</h3>
                    <ul>
                        <?php outputFiles('download/Bid Proforma'); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--JQUERY--> 
<script type="text/javascript" src="js/jquery.js"></script> 
<!--BOOTSTRAP--> 
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.js"></script> 
<script>
		$(function () {
			$('#myTab a').click(function (e) {
			e.preventDefault();
			$(this).tab('show');
			});	
		});
	</script> 
<script>
		// Javascript to enable link to tab
		var url = document.location.toString();
		if (url.match('#')) {
			$('.nav-tabs a[href=#'+url.split('#')[1]+']').tab('show') ;
		} 
		
		// Change hash for page-reload
		$('.nav-tabs a').on('shown', function (e) {
			window.location.hash = e.target.hash;
		});
	</script>
</body>
</html>
