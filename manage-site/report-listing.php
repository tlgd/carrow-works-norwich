<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Agents Insight - Dataroom</title>


<link href="css/dataroom-edit.css" rel="stylesheet" type="text/css" />

</head>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/slider.js"></script>
<body id="addCompany">
<div id="header">
<div id="header_wrapper">

                <div id="logo"><img src="images/logo.png"></div>

                <ul id="name">

					
                    <li class="white" style="width:80px;">Michael Avery</li>

                 
                                <li class="downarrow_1">Admin</li>
                                
                               <li class="white">|</li>
					   <li class="settings_1">Settings</li>
                               

    </ul>

				 <ul class="the_menu_1 the_menu">
                                    <span class="white1"><strong>Settings</strong></span>
                                    <li><a href="specification-listing.php">Edit Specifications</a></li>
                                    

    </ul>
<ul class="the_menu_2 the_menu">
                                    <span class="white1"><strong>Admin Links</strong></span>
                                    <li><a href="http://tlgd.zendesk.com" target="_blank">Support Website</a></li>
                                    <li><a href="mailto:support@tlgd.zendesk.com">Contact Support</a></li>
                                    <li><a href="/login/index.php?logout=1">Log out</a></li>

    </ul>




	


                
  </div>
</div>

<div id="body_wrapper">

<h1>Dataroom</h1>

 <ul id="breadcrumb">
    <li><a href="#">Dataroom /</a></li>
     <li><a href="#" class="active">Reports</a></li>            
    </ul>
    
       <div class="yellowunBound1">
            <div class="floatRight">
       		<a href="#">Cancel</a>
       </div>
           </div>
          

<div id="formContainer">

<div id="panel_nav_container">

		<div id="navigation">
        		<ul>
                    <li><a href="#">Files</a></li>
                    <li><a href="user-listing.php">Users</a></li>
                    <li><a href="#" class="active">Reports</a>
                    
                    	<ul>
                    	  <li><a href="#">Create Report</a></li>
                  	  	</ul>
                    
                  </li>
                </ul>
        </div>
</div>
 
   <div id="adminSideRight" class="marginb">        
   
   <div id="downarrow_grey_one"><img src="images/maindown_arrow_1.gif" width="25" height="10" /></div>
   
   	<h2>Your Reports</h2>
    
    <p>Reports enable you to see activity on both a user and file download basis. Create a report using the button below.<span class="yellowTxt"></span></p>
    
    <a id="create-report-btn" href="#" title="Create Report"><span>Create Report</span></a>
          
   </div>
   
   <div id="listcontainer">
   
   <table width="720" border="0" cellspacing="0" cellpadding="0" id="reportstable">
  <tr>
    <th colspan="5" scope="col">Recently Run Reports</th>
    </tr>
  <tr>
    <th scope="col" class="main" width="196">Report Name</th>
    <th scope="col" class="main" width="116">Type</th>
    <th scope="col" class="main" width="116">Date Range</th>
    <th scope="col" class="main" width="258">&nbsp;</th>
    <th width="34" class="main" scope="col">&nbsp;</th>
  </tr>
  <tr class="greytwo">
    <td>Reports name here</td>
    <td>File Report</td>
    <td>Month to Date</td>
    <td align="right"><a href="#">Run Now</a>&nbsp; | &nbsp;<a href="#">Create Similar</a></td>
    <td align="right"><a href="#"><img src="images/icons/cross_icon.gif" width="18" height="18" /></a></td>
  </tr>
  <tr class="greyone">
    <td>Reports name here</td>
    <td>File Report</td>
    <td>Month to Date</td>
    <td align="right"><a href="#">Run Now</a>&nbsp; | &nbsp;<a href="#">Create Similar</a></td>
    <td align="right"><a href="#"><img src="images/icons/cross_icon.gif" width="18" height="18" /></a></td>
  </tr>
  <tr class="greytwo">
    <td>Reports name here</td>
    <td>File Report</td>
    <td>Month to Date</td>
    <td align="right"><a href="#">Run Now</a>&nbsp; | &nbsp;<a href="#">Create Similar</a></td>
    <td align="right"><a href="#"><img src="images/icons/cross_icon.gif" width="18" height="18" /></a></td>
  </tr>
</table>

<div class="clear"></div>

<div id="bottom_sort">
        
        <p>Show rows:</p> 
        <form name="form" id="sort_rows" action="" method="get">
          <select name="to" id="to" onchange="MM_jumpMenu('parent',this,0)"> <!-- onchange="MM_jumpMenu('parent',this,0)" -->
            <option value="10" selected="">10</option>
            <option value="20">20</option>
            <option value="30">30</option>
          </select>
          <input name="from" type="hidden" value="0">
        </form>
        
        <div id="page_sort">
        
        <p>Showing 1 - 10</p>
        
        <a id="next" href="#" title="Next" onclick="MM_jumpMenu3('parent',this,0)"><span>Next</span></a>
        <a id="prev" href="#" title="Previous" onclick="MM_jumpMenu2('parent',this,0)"><span>Previous</span></a>
        
        </div>
        
        </div>
   
   </div>
   
   
   <div class="clear"></div>
   </div>

           
       <div class="yellowunBound1">
            <div class="floatRight">
       		<a href="#">Cancel</a>
       </div>
       </div>




</div>

<div id="footer">
<p><a href="#">Help ?</a></p>
</div>

</body>
</html>
