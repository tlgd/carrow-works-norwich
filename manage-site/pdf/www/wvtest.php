<?php
require_once(getenv('DOCUMENT_ROOT') . '/includes/common.php');
require_once(getenv('DOCUMENT_ROOT') . '/includes/class.property.php');

require_once("../dompdf_config.inc.php");

$html = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<style>

body {
  color:#000;
  font-family:Arial, Helvetica, sans-serif;
  font-size:12px;
  padding:6px;
  margin:0;
}

.circle li { 
list-style-type: circle; 
padding: 0;

}

table.info td{
	border:1px solid #000;
	background-color:#fff;
}

</style>

</head>

<body>

<script type="text/php">

if ( isset($pdf) ) {

  $font = Font_Metrics::get_font("Arial");;
  $size = 6;
  $color = array(0,0,0);
  $text_height = Font_Metrics::get_font_height($font, $size);

  $foot = $pdf->open_object();
  
  $w = $pdf->get_width();
  $h = $pdf->get_height();

  // Draw a line along the bottom
  $y = $h - $text_height - 24;
  $pdf->line(16, $y, $w - 16, $y, $color, 0.5);

  $pdf->close_object();
  $pdf->add_object($foot, "all");

  $text = "Page {PAGE_NUM} of {PAGE_COUNT}";  

  // Center the text
  $width = Font_Metrics::get_text_width("Page 1 of 2", $font, $size);
  $pdf->page_text($w / 2 - $width / 2, $y, $text, $font, $size, $color);
  
}
</script>

<div style="width:578px; height:829px;">

<div style="width:578px; height:829px; margin-bottom:6px; background-color:#000;">
 <table width="578" border="0" cellspacing="0" cellpadding="0" style="padding:0; margin:0; background-color:#000; width:578px; ">
  <tr>
    <td style="width:300px; height:72px;"><img src="/var/www/vhosts/wadhamandisherwood.co.uk/httpdocs/find-a-property/pdf/www/images/wandilogo.jpg" width="171px" height="72px" style="padding:0; margin:0; width:171px; height:72px;" /></td>
    

  </tr>
  <tr>
  	<td style="width:300px; height:72px;">
    
  	<p style="font-family:sans-serif; color:#999; font-size:15px; padding:40px 0 0 15px; margin:0; text-align:left;"><strong>Website User &amp; Download Report</strong><br />DATE RANGE: FROM - TO
</p>
    
  	</td>

  </tr>
</table>
</div>


<div style="width:558px;background-color:#000; padding:10px;text-align:center; margin-bottom:10px;font-size:15px; font-family:sans-serif; color:#fff;" >
There have been _ active users on the website to date
</div>


<div style="width:568px; padding:5px; text-align:center; color:#fff; background-color:#000; font-size:10px; font-family:sans-serif;" >
Visitor Overview
</div>

<div style="width:578px;font-size:10px; font-family:sans-serif; margin-bottom:10px; background-color:#000;">
<table width="578" class="info" cellpadding="0" cellspacing="0" border="0">
<tr>
<td>Company</td>
<td>Person</td>
<td>Login Count</td>
<td>Page Views</td>
<td>Downloads</td>
</tr>
<tr>
<td>-</td>
<td>-</td>
<td>-</td>
<td>-</td>
<td>-</td>
</tr>
</table>

</div>





</div>

</body> </html>

';
/*
function GetImageFromUrl($link)
 
{
 
$ch = curl_init();
 
curl_setopt($ch, CURLOPT_POST, 0);
 
curl_setopt($ch,CURLOPT_URL,$link);
 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
 
$result=curl_exec($ch);

curl_close($ch);
 
return $result;
 
}
*/
//sourcecode=GetImageFromUrl('http://maps.google.com/maps/api/staticmap?center='.htmlspecialchars($lat.','.$lon).'&zoom=14&size=291x194&markers=color:red%7C'.htmlspecialchars($lat.','.$lon).'&sensor=false');
//$savefile = fopen('/var/www/vhosts/wadhamandisherwood.co.uk/httpdocs/find-a-property/pdf/www/images/staticmap.png', 'w');
//fwrite($savefile, $sourcecode);
//fclose($savefile);

$dompdf = new DOMPDF();
//echo $html;
$dompdf->load_html($html);
$dompdf->render();

//$pdfoutput = $dompdf->output();
//$filename = "sample.pdf";
//$fp = fopen($filename, "a");
//fwrite($fp, $pdfoutput);
//fclose($fp); 


$dompdf->stream("test.pdf");

//include 'PDFMerger.php';

//$pdf = new PDFMerger;

//$pdf->addPDF('samplepdfs/one.pdf', '1, 3, 4')
	//->addPDF('samplepdfs/two.pdf', '1-2')
	//->addPDF('samplepdfs/three.pdf', 'all')
	//->merge('file', 'samplepdfs/TEST2.pdf');
	
	//REPLACE 'file' WITH 'browser', 'download', 'string', or 'file' for output options
	//You do not need to give a file path for browser, string, or download - just the name.

?>

